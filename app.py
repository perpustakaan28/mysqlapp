from models.customers import database
from controller import customers
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "testkunci"
jwt = JWTManager(app)
@app.route("/users", methods=["POST"])

def showUsers():
    return customers.shows()

@app.route("/user", methods=["POST"])
def showUser():
    param = request.json
    return customers.show(**params)


if __name__ == "__main__":
    app.run(debug=True)
