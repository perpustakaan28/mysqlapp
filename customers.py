from models.customers import database
from flask import Flask, jsonify, request

mysqldb = database()


def shows():
    dbresult = mysqldb.showUsers()
    result = []
    print(dbresult)
    for items in dbresult:
        user = {
            "id" : items[0],
            "username" : items[1],
            "firstname" : items[2],
            "lastname" : items[3],
            "email" : items[4]            
        }
        result.append(user)
        
    return jsonify(result)

def show(**params):
    dbresult = mysqldb.showUserById(**params)
    user = {
        "id" : dbresult[0],
        "username" : dbresult[1],
        "firstname" : dbresult[2],
        "lastname" : dbresult[3],
        "email" : dbresult[4]            
    }
        
    return jsonify(user)
